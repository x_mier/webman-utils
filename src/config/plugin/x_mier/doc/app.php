<?php
return [
    'enable' => true,
    'module' => [],
    'alisms' => [
        'accessKeyId' => '', // 阿里云短信服务
        'accessKeySecret' => '', // 阿里云短信服务
        'signName' => '', // 阿里云短信签名
        'templateCode' => '' // 阿里云短信模板
    ],
    'wx_login' => [
        // APP
        'appId' => '',
        'appSecret' => '',
        // 小程序
        'appId2' => '',
        'appSecret2' => '',
    ],
    'key' => [
        'key' => '', //AES加密key
        'keyShare' => '', //AES加密key分享
        'server_public_key' => '', //RSA加密公钥服务端
        'server_private_key' => '', //RSA加密私钥服务端
        'client_public_key' => '', //RSA加密公钥客户端
        'client_private_key' => '', //RSA加密私钥客户端
    ],
];
