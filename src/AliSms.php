<?php

namespace x_mier\utils;

class AliSms
{
    private $accessKeyId = ''; // Access Key ID
    private $accessKeySecret = ''; // Access Access Key Secret
    private $signName = ''; // 签名
    private $templateCode = ''; // 模版ID
    public function __construct()
    {
        $config = config('plugin.x_mier.doc.app');
        if ($config && !empty($config['alisms'])) {
            $config = $config['alisms'];
            $this->accessKeyId = $config['accessKeyId'];
            $this->accessKeySecret = $config['accessKeySecret'];
            $this->signName = $config['signName'];
            $this->templateCode = $config['templateCode'];
        }
    }
    /**
     * @title  发送短信验证码
     */
    public function send_msg($phone)
    {
        try {
            $key = 'sms_code_' . $phone;
            $data = cache($key);
            if ($data && time() - $data['time'] < 60) {
                throw new \Exception('请勿重复发送');
            }
            $code = rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9);
            $result = $this->send_verify($phone, $code);
            if (!$result) {
                throw new \Exception('发送失败！');
            }
            cache($key, ['code' => $code, 'time' => time()], 5 * 60);
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }
    //验证验证码
    public function check_code($phone, $code)
    {
        $key = 'sms_code_' . $phone;
        $sms_code = cache($key);
        if (empty($sms_code['code'])) {
            return '请先发送验证码!';
        }
        if ($sms_code['code'] != $code) {
            return '验证码错误!';
        }
        //验证码验证成功后删除验证码
        cache($key, null);
        return true;
    }
    private function percentEncode($string)
    {
        $string = urlencode($string);
        $string = preg_replace('/\+/', '%20', $string);
        $string = preg_replace('/\*/', '%2A', $string);
        $string = preg_replace('/%7E/', '~', $string);
        return $string;
    }
    /**
     * 签名
     *
     * @param array $parameters            
     * @param string $accessKeySecret            
     * @return string
     */
    private function computeSignature($parameters, $accessKeySecret)
    {
        ksort($parameters);
        $canonicalizedQueryString = '';
        foreach ($parameters as $key => $value) {
            $canonicalizedQueryString .= '&' . $this->percentEncode($key) . '=' . $this->percentEncode($value);
        }
        $stringToSign = 'GET&%2F&' . $this->percentencode(substr($canonicalizedQueryString, 1));
        $signature = base64_encode(hash_hmac('sha1', $stringToSign, $accessKeySecret . '&', true));
        return $signature;
    }
    /**
     * @param string $mobile            
     * @param string $verify_code            
     */
    public function send_verify($mobile, $verify_code)
    {
        $params = array(
            'SignName' => $this->signName,
            'Format' => 'JSON',
            'Version' => '2017-05-25',
            'AccessKeyId' => $this->accessKeyId,
            'SignatureVersion' => '1.0',
            'SignatureMethod' => 'HMAC-SHA1',
            'SignatureNonce' => uniqid(),
            'Timestamp' => gmdate('Y-m-d\TH:i:s\Z'),
            'Action' => 'SendSms',
            'TemplateCode' => $this->templateCode,
            'PhoneNumbers' => $mobile,
            'TemplateParam' => '{"code":"' . $verify_code . '"}'
        );
        // 计算签名并把签名结果加入请求参数
        $params['Signature'] = $this->computeSignature($params, $this->accessKeySecret);
        $http = new \Workerman\Http\Client();
        $url = 'http://dysmsapi.aliyuncs.com/?' . http_build_query($params);
        $http->get($url, function ($response) use ($mobile) {
            $result = json_decode($response->getBody()->getContents(), true);
            if (isset($result['Code']) && $result['Code'] != 'OK') {
                $error = $this->getErrorMessage($result['Code']);
                $file = run_path('/runtime/logs/') . 'alisms-' . date('Y-m-d') . '.log';
                file_put_contents($file, '[' . $mobile . ']' . "\n" . var_export($error, true) . "\n", FILE_APPEND);
            }
        });
    }
    /**
     * 获取详细错误信息
     * @param unknown $status            
     */
    public function getErrorMessage($status)
    {
        // https://api.alidayu.com/doc2/apiDetail?spm=a3142.7629140.1.19.SmdYoA&apiId=25450
        $message = array(
            'InvalidDayuStatus.Malformed' => '账户短信开通状态不正确',
            'InvalidSignName.Malformed' => '短信签名不正确或签名状态不正确',
            'InvalidTemplateCode.MalFormed' => '短信模板Code不正确或者模板状态不正确',
            'InvalidRecNum.Malformed' => '目标手机号不正确，单次发送数量不能超过100',
            'InvalidParamString.MalFormed' => '短信模板中变量不是json格式',
            'InvalidParamStringTemplate.Malformed' => '短信模板中变量与模板内容不匹配',
            'InvalidSendSms' => '触发业务流控',
            'InvalidDayu.Malformed' => '变量不能是url，可以将变量固化在模板中'
        );
        if (isset($message[$status])) {
            return $message[$status];
        }
        return $status;
    }
}
